# WEBprog

---
title: Курсовая работа
---
# Стиль: 
- ### верстальщик;

# Предпочитаемый:
- ### JavaScript;

# Раздел IT: 
- ### WEB;
сервис для разработки интерфейсов и прототипирования
Изображение (лого GitLab):

![alt text](https://im0-tub-kz.yandex.net/i?id=bb2b7806416fb3982a3a3c0838cd0a6d&n=13 "Title Text")

Ссылка на поисковик Google [GOOGLE LINK](https://www.google.com)
- https://www.google.com

Пример таблицы markdown:

| Заголовок 1 | Заголовок 2 | Заголовок 3 |
| ---         |  -----:  |:--------:|
| ячейка 1    | ячейка 2 | ячейка 3   |
| ячейка 4    | ячейка 5 is longer | cell 6 is much longer than the others, but that's ok. It will eventually wrap the text when the cell is too large for the display size. |
| ячейка 7    |          | ячейка <br> 9 |

# Заголовок 1
## Заголовок 2
### Заголовок 3
#### Заголовок 4
##### Заголовок 5
###### Заголовок 6
